from marshmallow import fields, Schema

from domain.meta_info.github_language_schema import GithubLanguageSchema


class GithubProfileSchema(Schema):
    profile_type = fields.Str()
    github_id = fields.Str()
    name = fields.Str()
    avatar_url = fields.Str()
    bio = fields.Str()
    location = fields.Str()
    profile_link = fields.Str()
    languages = fields.List(fields.Nested(GithubLanguageSchema))
