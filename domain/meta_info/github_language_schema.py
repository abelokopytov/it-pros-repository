from marshmallow import Schema, fields


class GithubLanguageSchema(Schema):
    name = fields.Str()
    rate = fields.Number()
    repositories = fields.List(fields.Str(), allow_none=True)
