from marshmallow import fields


class CreatedAtField(fields.DateTime):

    def _deserialize(self, value, attr, data):
        return value.generation_time
