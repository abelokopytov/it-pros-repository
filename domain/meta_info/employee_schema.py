from marshmallow import fields, post_load, Schema

from domain.meta_info.any_profile_schema import AnyProfileSchema
from domain.meta_info.base_schema import BaseSchema
from domain.models.employee import Employee


class EmployeeSchema(Schema, BaseSchema):
    email = fields.Str()
    profiles = fields.List(fields.Nested(AnyProfileSchema), allow_none=True)

    @post_load
    def make_employee(self, data):
        return Employee(**data)
