from marshmallow import fields


class EntityIdField(fields.Field):
    def _serialize(self, value, attr, obj):
        if value is None:
            return ""
        return value

    def _deserialize(self, value, attr, data):
        return str(value)
