from marshmallow import fields

from domain.meta_info.created_at_field import CreatedAtField
from domain.meta_info.object_id_field import EntityIdField


class BaseSchema:
    entity_id = EntityIdField(load_from="_id", load_only=True)
    created_at = CreatedAtField(load_from="_id", load_only=True)
    updated_at = fields.DateTime(allow_none=True)
