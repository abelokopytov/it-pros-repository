from marshmallow_oneofschema import OneOfSchema

from domain.meta_info.github_profile_schema import GithubProfileSchema


class AnyProfileSchema(OneOfSchema):
    type_schemas = {
        'GithubProfile': GithubProfileSchema
    }

    def get_obj_type(self, obj):
        if isinstance(obj, dict) and obj.get("profile_type") == "github":
            return "GithubProfile"
        else:
            raise Exception('Unknown object type: %s' % obj.__class__.__name__)
