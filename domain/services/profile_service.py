import inject as inject

from data_access.repositories.employee_repository import EmployeeRepository


class ProfileService:
    providers = []

    @inject.params(employee_repository=EmployeeRepository)
    def __init__(self, employee_repository):
        self.employee_repository = employee_repository

    def add_provider(self, provider):
        self.providers.append(provider)

    def fetch_profiles(self, user_id):
        fetched_profiles = []
        employee = self.employee_repository.find_by_id(user_id)
        for provider in self.providers:
            profiles = provider.search(employee.email)
            fetched_profiles.extend(profiles)

        self.employee_repository.update_profiles(employee.entity_id, fetched_profiles)
        return fetched_profiles
