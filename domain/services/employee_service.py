from datetime import datetime
import inject as inject

from data_access.repositories.employee_repository import EmployeeRepository
from domain.models.employee import Employee
from scheduler.celery_app import CeleryApp


class EmployeeService:
    @inject.params(employee_repository=EmployeeRepository, celery_app=CeleryApp)
    def __init__(self, employee_repository, celery_app):
        self.employee_repository = employee_repository
        self.celery_app = celery_app

    def get_by_email(self, email):
        employee = self.employee_repository.find_one(dict(email=email))
        if not employee:
            employee = Employee(email)
            employee_id = self.employee_repository.add(employee)
            self.celery_app.send_task("fetch_profiles", kwargs=dict(employee_id=employee_id))
            return employee

        return employee

    def get_by_id(self, id_):
        employee = self.employee_repository.find_by_id(id_)
        return employee

    def update(self, employee):
        employee.updated_at = datetime.today()
        self.employee_repository.update(employee)
        return employee
