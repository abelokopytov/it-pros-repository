class BaseModel:
    def __init__(self, entity_id, created_at, updated_at=None):
        self.updated_at = updated_at
        self.entity_id = entity_id
        self.created_at = created_at
