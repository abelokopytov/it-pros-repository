from domain.models.base_model import BaseModel


class Employee(BaseModel):
    def __init__(self, email, entity_id=None, created_at=None, profiles=None, updated_at=None):
        super().__init__(entity_id, created_at, updated_at)

        self.email = email
        self.profiles = [] if not profiles else profiles
