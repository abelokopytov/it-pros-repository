from collections import namedtuple

GithubLanguage = namedtuple("GithubLanguage", "name repositories rate")
