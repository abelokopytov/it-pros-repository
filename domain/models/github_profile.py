from collections import namedtuple

GithubProfile = namedtuple("GithubProfile", "github_id name avatar_url bio location profile_link languages")