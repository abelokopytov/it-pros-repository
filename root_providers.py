from data_access.providers import config as data_access_providers
from scheduler.providers import config as scheduler_providers


def providers_config(binder):
    binder.install(data_access_providers)
    binder.install(scheduler_providers)