from unittest.mock import Mock

from domain.models.employee import Employee

employee_repository_mock = Mock()
_employee = Employee("test@test.com")
_attrs = {
    "find_by_id.return_value": _employee,
    "find_one.side_effect": lambda query: _employee if query.get("email") == "test@test.com" else None,
    "find_all.return_value": (_employee,)
}
employee_repository_mock.configure_mock(**_attrs)
