import unittest
import inject

from data_access.repositories.employee_repository import EmployeeRepository
from domain.models.employee import Employee
from domain.services.employee_service import EmployeeService
from scheduler.celery_app import CeleryApp
from tests.mocks.celery_app_mock import celery_app_mock
from tests.mocks.employee_repository_mock import employee_repository_mock


def setup_providers(binder):
    binder.bind(EmployeeRepository, employee_repository_mock)
    binder.bind(CeleryApp, celery_app_mock)


class EmployeeServiceTest(unittest.TestCase):
    def setUp(self):
        inject.clear_and_configure(setup_providers)
        self.employee_service = EmployeeService()

    def test_constructor(self):
        self.assertIsInstance(self.employee_service, EmployeeService)

    def test_get_by_email_exist(self):
        employee = self.employee_service.get_by_email("test@test.com")
        self.assertEqual(employee.email, "test@test.com")

    def test_get_by_email_doesnt_exist(self):
        employee = self.employee_service.get_by_email("anonim@test.com")
        self.assertEqual(employee.email, "anonim@test.com")
        celery_app_mock.send_task.assert_called_once()

    def test_get_by_id(self):
        employee = self.employee_service.get_by_id("nnn")
        self.assertEqual(employee.email, "test@test.com")

    def test_update(self):
        profiles = dict(github=dict(id=1))
        employee = Employee(email="test@test.com", profiles=profiles)
        employee_updated = self.employee_service.update(employee)
        self.assertIsNotNone(employee_updated.updated_at)

    def tearDown(self):
        inject.clear()


if __name__ == "__main__":
    unittest.main()
