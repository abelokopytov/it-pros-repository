import unittest
from unittest.mock import Mock

import inject

from data_access.repositories.employee_repository import EmployeeRepository
from domain.services.profile_service import ProfileService
from tests.mocks.employee_repository_mock import employee_repository_mock


def setup_providers(binder):
    binder.bind(EmployeeRepository, employee_repository_mock)


class ProfileServiceTest(unittest.TestCase):

    def setUp(self):
        inject.clear_and_configure(setup_providers)
        self.profile_service = ProfileService()
        self.dumb_provider = Mock()
        dumb_api = {
            "search.return_value": ("dumb_api", dict())
        }
        self.dumb_provider.configure_mock(**dumb_api)

    def test_constructor(self):
        self.assertIsInstance(self.profile_service, ProfileService)

    def test_add_provider(self):
        self.profile_service.add_provider(self.dumb_provider)
        self.assertEqual(len(self.profile_service.providers), 1)

    def test_fetch_profiles(self):
        user_id = "nnn"
        self.profile_service.add_provider(self.dumb_provider)
        fetched_profiles = self.profile_service.fetch_profiles(user_id)
        self.assertGreater(len(fetched_profiles), 0)

    def tearDown(self):
        inject.clear()


if __name__ == "__main__":
    unittest.main()
