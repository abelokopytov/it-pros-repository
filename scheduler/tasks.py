import inject

from data_access.api_providers.github_provider import GithubProvider
from domain.services.profile_service import ProfileService
from scheduler.celery_app import CeleryApp
from root_providers import providers_config

inject.configure_once(providers_config)

app = inject.instance(CeleryApp)
github_provider = inject.instance(GithubProvider)
profile_service = ProfileService()
profile_service.add_provider(github_provider)


@app.task(name="fetch_profiles")
def fetch_profiles(employee_id):
    profile_service.fetch_profiles(employee_id)
