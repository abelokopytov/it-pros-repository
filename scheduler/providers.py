from celery import Celery

import config
from scheduler.celery_app import CeleryApp

celery_app = Celery(broker=config.celery_broker_url)


def config(binder):
    binder.bind(CeleryApp, celery_app)
