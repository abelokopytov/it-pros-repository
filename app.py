import inject
from flask import (Flask, jsonify)

from root_providers import providers_config

inject.configure_once(providers_config)

from domain.meta_info.employee_schema import EmployeeSchema
from domain.services.employee_service import EmployeeService

app = Flask(__name__)
employee_schema = EmployeeSchema(exclude=("updated_at",))
employee_service = EmployeeService()


@app.route("/employee/<email>", methods=["GET"])
def get_employee(email):
    # TODO: validate email
    employee = employee_service.get_by_email(email)

    if employee:
        return jsonify({
            "employee": employee_schema.dump(employee).data
        })
    else:
        return jsonify({})


if __name__ == "__main__":
    app.run(debug=True)
