import os

from os.path import join, dirname
from dotenv import load_dotenv

dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)

github_username = os.environ['GITHUB_USER']
github_password = os.environ['GITHUB_PASSWORD']
mongodb_host = os.environ['MONGODB_HOST']
mongodb_port = os.environ['MONGODB_PORT']
mongodb_name = os.environ['MONGODB_NAME']
celery_broker_url = os.environ['CELERY_BROKER_URL']
