from pymongo import MongoClient

import config


class MongoContextFactory:

    @staticmethod
    def get_context():
        client = MongoClient(config.mongodb_host, int(config.mongodb_port))
        return client[config.mongodb_name]
