from github import Github


class GithubProvider:
    _profile_type = "github"

    def __init__(self, user, password):
        self.github_api = Github(user, password)

    def search(self, email):
        profiles = []
        users = self.github_api.search_users(f"{email} in:email type:user")
        for user in users:

            # repos user contributed to
            repos_with_contribution = []
            languages = dict()
            repos = user.get_repos()
            for repo in repos:
                contributors = repo.get_contributors()
                if user in contributors:
                    repos_with_contribution.append(repo)

                    # add repo's languages to global list
                    if not repo.language:
                        continue

                    if repo.language not in languages:
                        languages[repo.language] = dict(name=repo.language, repositories=list(), rate=0)

                    languages[repo.language]["repositories"].append(repo.name)
                    languages[repo.language]["rate"] += 1

            user_profile = dict(
                profile_type=self._profile_type,
                github_id=user.id,
                name=user.name,
                avatar_url=user.avatar_url,
                bio=user.bio,
                location=user.location,
                profile_link=user.html_url,
                languages=languages.values())
            profiles.append(user_profile)

        return profiles
