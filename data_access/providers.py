import config
from data_access.api_providers.github_provider import GithubProvider
from data_access.infrastructure.db_context import DbContext
from data_access.infrastructure.mongo_context_factory import MongoContextFactory
from data_access.repositories.employee_repository import EmployeeRepository

github_provider = GithubProvider(config.github_username, config.github_password)
db_context = MongoContextFactory.get_context()
employee_repository = EmployeeRepository(db_context)


def config(binder):
    binder.bind(GithubProvider, github_provider)
    binder.bind(DbContext, db_context)
    binder.bind(EmployeeRepository, employee_repository)
