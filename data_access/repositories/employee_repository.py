from bson import ObjectId

from domain.meta_info.any_profile_schema import AnyProfileSchema
from domain.meta_info.employee_schema import EmployeeSchema


class EmployeeRepository:
    collection_name = "employee"
    employee_schema = EmployeeSchema()
    profile_schema = AnyProfileSchema()

    def __init__(self, db_context):
        self.db = db_context
        self._collection = self.db[self.collection_name]

    def find_by_id(self, entity_id):
        return self.find_one({"_id": ObjectId(entity_id)})

    def find_one(self, query):
        employee_data = self._collection.find_one(query)
        if employee_data:
            return self.employee_schema.load(employee_data).data
        return None

    def find_all(self, query):
        result = []
        employee_data = self._collection.find(query)

        if employee_data.count():
            for employee in employee_data:
                result.append(self.employee_schema.load(employee).data)

        return result

    def add(self, employee):
        result = self._collection.insert_one(self.employee_schema.dump(employee).data)
        entity_id = result.inserted_id
        return str(entity_id)

    def update(self, employee):
        result = self._collection.update_one({"_id": ObjectId(employee.entity_id)},
                                             {"$set": self.employee_schema.dump(employee).data})
        return str(result.upserted_id)

    def update_profiles(self, employee_id, profiles):
        result = self._collection.update_one({"_id": ObjectId(employee_id)},
                                             {"$set": {"profiles": self.profile_schema.dump(profiles, many=True).data}})
        return str(result.upserted_id)
